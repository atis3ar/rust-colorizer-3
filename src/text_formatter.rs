#![allow(
    dead_code,
    unused_imports,
    unused_assignments,
    unused_variables,
    unused_mut
)]

use log::{debug, error, info, trace, warn};
use regex::Regex;
use std::borrow::{Borrow, BorrowMut};
use std::ops::{Index, Range};

#[derive(Debug, Copy, Clone)]
pub struct RgbColor {
    pub r: i32,
    pub g: i32,
    pub b: i32,
}

pub fn format_ansi_simple_color(text: &str, color: i32) -> String {
    return format!("\x1b[38;5;{}m{}\x1b[m", color, text);
}

pub fn format_rgb_ansi(text: &str, color: &RgbColor) -> String {
    return format!(
        "\x1b[38;2;{};{};{}m{}\x1b[m",
        color.r, color.g, color.b, text,
    );
}

#[derive(Debug)]
pub struct Formatting {
    range: Range<usize>,
    color: Option<RgbColor>,
}

#[derive(Debug)]
pub struct TextFormatter {
    formations: Vec<Formatting>,
    text: String,
    simple_color_i: usize,
}

impl TextFormatter {
    pub fn new(text: &str) -> Self {
        let re: regex::Regex = regex::Regex::new("\x1b.*?\x1b\\[m").unwrap();
        let res = re.find_iter(text);
        let mut parts: Vec<Formatting> = Vec::new();
        let mut previous_end: usize = 0;

        for x in res {
            if previous_end < x.start() {
                parts.push(Formatting {
                    range: Range {
                        start: previous_end,
                        end: x.start(),
                    },
                    color: None,
                });
            }

            parts.push(Formatting {
                range: Range {
                    start: x.start(),
                    end: x.end(),
                },
                color: None,
            });
            previous_end = x.end();
        }

        return TextFormatter {
            text: text.to_string(),
            formations: parts,
            simple_color_i: 0,
        };
    }

    fn merge_formations(&mut self, mut new_formations: Vec<Formatting>) {
        debug!("merge_formations: start: formations={:?}", self.formations);
        debug!(
            "merge_formations: start: new_formations={:?}",
            new_formations
        );

        loop {
            let mut formations_to_remove: Vec<usize> = Vec::new();
            let mut new_new_formations: Vec<Formatting> = Vec::new();

            for mut new_formation in new_formations.iter() {
                let mut i: usize = 0;
                for mut old_formation in self.formations.iter_mut() {
                    let old_start = old_formation.range.start;
                    let old_end = old_formation.range.end;
                    let new_start = new_formation.range.start;
                    let new_end = new_formation.range.end;

                    if old_end < new_start || old_start > new_end {
                        continue;
                    }

                    if new_formation.range.contains(&old_start)
                        && new_formation.range.contains(&old_end)
                    {
                        formations_to_remove.push(i);
                        continue;
                    }

                    if old_end >= new_start && old_end <= new_end {
                        old_formation.range.end = new_start;
                    }

                    if old_start >= new_start && old_start <= new_end {
                        old_formation.range.start = new_end;
                    }

                    if old_formation.range.contains(&new_start) {
                        old_formation.range.end = new_start;
                        if old_end > new_end {
                            // Have to split into 2 - [0..5] + [2..3] = [0..2],[2..3],[3..5]
                            new_new_formations.push(Formatting {
                                range: Range {
                                    start: new_end,
                                    end: old_end,
                                },
                                color: old_formation.color.clone(),
                            });
                        }
                    }

                    i += 1;
                }
            }

            formations_to_remove.reverse();
            for i in formations_to_remove {
                self.formations.remove(i);
            }

            let is_done = new_new_formations.is_empty();
            for formation in new_new_formations {
                self.formations.push(formation);
            }

            self.formations.sort_by(|a, b| {
                return a.range.start.cmp(&b.range.start);
            });

            if is_done {
                break;
            }
        }

        for formation in new_formations {
            self.formations.push(formation);
        }

        self.formations.sort_by(|a, b| {
            return a.range.start.cmp(&b.range.start);
        });

        let mut colored_formations: Vec<&Formatting> = Vec::new();
        for formation in &self.formations {
            if formation.color.is_some() {
                colored_formations.push(&formation);
            }
        }

        debug!("merge_formations: end: formations{:?}", self.formations);
    }

    pub fn set_text_color(&mut self, text: &str, color: &RgbColor) {
        debug!("set_text_color: text={} color={:?}", text, color);
        let mut new_formations: Vec<Formatting> = Vec::new();
        let mut old_pos: usize;
        let mut pos: usize = 0;

        let len_text = text.len();
        let len_needle: usize = text.len();
        let len_haystack: usize = self.text.len();

        let lower_needle = text.to_lowercase();
        let lower_haystack = self.text.to_lowercase();

        loop {
            old_pos = pos;
            if pos + len_needle > len_haystack {
                break;
            }

            let temp_pos = lower_haystack[pos..len_haystack].find(&lower_needle);
            match temp_pos {
                Some(value) => {
                    pos += value;
                }

                None => {
                    break;
                }
            }

            new_formations.push(Formatting {
                color: Some(color.clone()),
                range: Range {
                    start: pos,
                    end: pos + len_needle,
                },
            });
            pos += len_needle;
        }

        if !new_formations.is_empty() {
            self.merge_formations(new_formations);
        }
    }

    pub fn set_pattern_match_color(&mut self, pattern: &Regex, color: &RgbColor) {
        debug!(
            "set_pattern_matches_color: pattern={} color={:?}",
            pattern.as_str(),
            color
        );
        let mut new_formations: Vec<Formatting> = Vec::new();
        for x in pattern.find_iter(&self.text) {
            new_formations.push(Formatting {
                color: Some(color.clone()),
                range: Range {
                    start: x.start(),
                    end: x.end(),
                },
            });
        }

        if !new_formations.is_empty() {
            self.merge_formations(new_formations);
        }
    }

    pub fn set_pattern_capture_color(&mut self, pattern: &Regex, color: &RgbColor) {
        debug!(
            "set_pattern_captures_color: pattern={} color={:?}",
            pattern.as_str(),
            color
        );
        let mut new_formations: Vec<Formatting> = Vec::new();

        let captures = pattern.captures(&self.text);
        if captures.is_some() {
            for (i, capture) in captures.unwrap().iter().enumerate() {
                if i == 0 {
                    continue;
                }

                if capture.is_some() {
                    let x = capture.unwrap();
                    // debug!("Match: {} [{}..{}]", x.as_str(), x.start(), x.end());
                    new_formations.push(Formatting {
                        color: Some(color.clone()),
                        range: Range {
                            start: x.start(),
                            end: x.end(),
                        },
                    });
                }
            }
        }

        if !new_formations.is_empty() {
            self.merge_formations(new_formations);
        }
    }

    pub fn format(&mut self, use_rgb: bool) -> String {
        if self.formations.is_empty() {
            return self.text.to_string();
        }

        debug!(
            "format: text.trimmed={} formations={:?}",
            self.text.trim_end(),
            self.formations
        );
        let mut i: usize = 0;
        let mut result = String::new();

        for formation in self.formations.iter() {
            if formation.range.start > i {
                result += &self.text[i..formation.range.start];
            }

            if formation.color.is_some() {
                #[cfg(debug_assertions)]
                if formation.range.start > formation.range.end {
                    error!(
                        "Invalid formation range. range={:?} color={:?} text={:?}",
                        formation.range, &formation.color, self.text,
                    );
                    continue;
                }

                if use_rgb {
                    result += &format_rgb_ansi(
                        &self.text[formation.range.start..formation.range.end],
                        &formation.color.as_ref().unwrap(),
                    );
                } else {
                    result += &format_ansi_simple_color(
                        &self.text[formation.range.start..formation.range.end],
                        formation.color.as_ref().unwrap().r,
                    );
                }
            } else {
                result += &self.text[formation.range.start..formation.range.end];
            }

            i = formation.range.end;
        }

        let text_len = self.text.len();
        if i < text_len {
            result += &self.text[i..text_len];
        }

        return result;
    }
}
