#![allow(dead_code, unused_imports, unused_assignments, unused_variables)]

mod colors;
mod text_formatter;

use crate::colors::rgb_colors::RGB_COLORS;
use crate::colors::zsh_colors::ZSH_COLORS;
use crate::text_formatter::{format_rgb_ansi, Formatting, RgbColor, TextFormatter};

use argparse::{ArgumentParser, List, Store, StoreTrue};
use log::Level::Error;
use log::LevelFilter;
use regex::Regex;
use std::borrow::{Borrow, BorrowMut};
use std::collections::hash_set::Union;
use std::mem::ManuallyDrop;
use std::process::exit;
use std::{env, io, time};
use argparse::action::Action::Flag;
use argparse::action::Action;

#[derive(Debug, Clone)]
enum ColoringPattern {
    RawText { text: String, color: RgbColor },
    PatternMatch { pattern: Regex, color: RgbColor },
    PatternCapture { pattern: Regex, color: RgbColor },
}

impl ColoringPattern {
    pub fn new_raw_text(color: &RgbColor, text: &str) -> Self {
        return ColoringPattern::RawText {
            color: color.clone(),
            text: text.to_string(),
        };
    }

    pub fn new_pattern_match(color: &RgbColor, pattern: Regex) -> Self {
        return ColoringPattern::PatternMatch {
            color: color.clone(),
            pattern,
        };
    }

    pub fn new_pattern_capture(color: &RgbColor, pattern: Regex) -> Self {
        return ColoringPattern::PatternCapture {
            color: color.clone(),
            pattern,
        };
    }
}

fn format_output_string(
    text: &str,
    patterns: &Vec<ColoringPattern>,
    use_rgb_colors: bool,
) -> String {
    let mut formatter = TextFormatter::new(&text);
    for item in patterns {
        match item {
            ColoringPattern::RawText { text, color } => {
                formatter.set_text_color(text, color);
            }
            ColoringPattern::PatternMatch { pattern, color } => {
                formatter.set_pattern_match_color(pattern, color);
            }
            ColoringPattern::PatternCapture { pattern, color } => {
                formatter.set_pattern_capture_color(pattern, color);
            }
        }
    }

    return formatter.format(use_rgb_colors);
}

fn string_letter_at(text: &str, position: usize) -> String {
    for (i, char) in text.chars().enumerate() {
        if i == position {
            return char.to_string();
        }
    }
    return "".to_string();
}

fn main() {
    // Default rust log level is Error
    // Set env variables to change it "RUST_LOG={debug|trace|info|warning}"
    env_logger::init();

    let red0 = &RgbColor {
        r: 200,
        g: 50,
        b: 50,
    };
    println!(
        "{}",
        format_rgb_ansi(
            "######################## Start ########################",
            red0
        ),
    );
    // Later check if is zsh
    // if std::env::var_os("ZSH_VERSION").is_some() {

    let mut use_stdin = false;
    let mut use_rgb_colors = false;
    let mut input_buffer = String::new();
    let mut search_strings: Vec<String> = Vec::new();

    {
        // this block limits scope of borrows by ap.refer() method
        let mut parser = ArgumentParser::new();
        parser.set_description("Description");
        parser
            .refer(&mut search_strings)
            .add_argument("patterns", List, "Search patterns");
        parser
            .refer(&mut input_buffer)
            .add_option(&["-i", "--input"], Store, "Input text");
        parser
            .refer(&mut use_rgb_colors)
            .add_option(&["--rgb"], StoreTrue, "Use rgb color");
        parser.parse_args_or_exit();
    }

    if input_buffer.is_empty() {
        use_stdin = true;
    }

    println!("logLevel={:?}", log::max_level());

    let arg_text = format!(
        "Args: use_rgb={} use_stdin={} input={:?} search_strings{:?}",
        use_rgb_colors, use_stdin, input_buffer, search_strings,
    );
    println!("{}", format_rgb_ansi(&arg_text, red0));

    if search_strings.is_empty() {
        eprintln!("Search strings must be set");
        exit(1);
    }

    let mut colors_to_use = Vec::new();
    if use_rgb_colors {
        for color in &RGB_COLORS {
            colors_to_use.push(color.clone());
        }
    } else {
        for color in &ZSH_COLORS {
            colors_to_use.push(RgbColor {
                r: color.clone(),
                g: color.clone(),
                b: color.clone(),
            });
        }
    }

    let mut color_words: Vec<ColoringPattern> = Vec::new();
    let mut lines_read: usize = 0;
    let mut total_bytes_read: usize = 0;

    let mut i = 0;
    color_words = search_strings
        .iter()
        .map(|text| {
            let color: &RgbColor = &colors_to_use[i];
            let coloring_pattern: ColoringPattern;

            if text.starts_with("/m") {
                let re = regex::Regex::new(&text[2..text.len()]);
                if re.is_ok() {
                    coloring_pattern = ColoringPattern::new_pattern_match(color, re.unwrap());
                } else {
                    eprintln!("Invalid pattern: {}", text);
                    exit(2);
                }
            } else if text.starts_with("/c") {
                let re = regex::Regex::new(&text[2..text.len()]);
                if re.is_ok() {
                    coloring_pattern = ColoringPattern::new_pattern_capture(color, re.unwrap());
                } else {
                    eprintln!("Invalid pattern: {}", text);
                    exit(2);
                }
            } else {
                coloring_pattern = ColoringPattern::new_raw_text(color, text);
            }

            i += 1;
            if i == colors_to_use.len() {
                i = 0;
            }
            return coloring_pattern;
        })
        .collect();

    if use_stdin {
        let handle = io::stdin();
        loop {
            match handle.read_line(&mut input_buffer) {
                Ok(bytes_read) => {
                    if bytes_read == 0 {
                        break;
                    }

                    total_bytes_read += bytes_read;
                    lines_read += 1;
                    let output = format_output_string(&input_buffer, &color_words, use_rgb_colors);
                    print!("{}", output);
                    input_buffer.clear();
                }

                Err(error) => {
                    eprintln!("Error while reading line: {}", error);
                    break;
                }
            }
        }
    } else {
        let output = format_output_string(&input_buffer, &color_words, use_rgb_colors);
        print!("{}", output);
    }

    println!(" ");
}
