#![allow(
    dead_code,
    unused_imports,
    unused_assignments,
    unused_variables,
    unused_mut
)]

use crate::text_formatter::RgbColor;

pub const RGB_COLORS: [RgbColor; 7] = [
    RgbColor {
        r: 100,
        g: 242,
        b: 250,
    }, // light-blue
    RgbColor {
        r: 60,
        g: 255,
        b: 90,
    }, // green
    RgbColor {
        r: 255,
        g: 113,
        b: 154,
    }, // pinkish
    RgbColor {
        r: 235,
        g: 235,
        b: 110,
    }, // yellow
    RgbColor {
        r: 255,
        g: 150,
        b: 120,
    }, // orange
    RgbColor {
        r: 130,
        g: 175,
        b: 255,
    }, // blue
    RgbColor {
        r: 233,
        g: 110,
        b: 245,
    }, // purple
];
