#![allow(
    dead_code,
    unused_imports,
    unused_assignments,
    unused_variables,
    unused_mut
)]

use crate::text_formatter::RgbColor;

pub static ZSH_COLORS: [i32; 11] = [14, 10, 11, 111, 9, 208, 150, 138, 206, 70, 33];